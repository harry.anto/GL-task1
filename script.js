//validation

$(function () {
  $("#form").validate({
    rules: {
      name: "required",
      email: {
        required: true,
        email: true,
      },
      phone: {
        required: true,
        minlength: 10,
      },
      checkbox: "required",
    },
    messages: {
      name: "Please enter your name",
      email: "Please enter a valid email address",
      phone: {
        required: "Please provide a phone number",
        minlength: "Your phone number must be at least 10 characters long",
      },
      checkbox: "Please check the checkbox",
    },
    errorPlacement: function (error, element) {
      if (element.is(":checkbox")) {
        error.appendTo(element.parents(".form__checkbox"));
      } else {
        error.insertAfter(element);
      }
    }
  });
});

//accordion

const accordions = document.querySelectorAll(".curriculum__accordion");

for (const accordion of accordions) {
  const panels = accordion.querySelectorAll(".accordion__panel");
  for (const panel of panels) {
    const head = panel.querySelector(".accordion__header");
    head.addEventListener("click", () => {
      for (const otherPanel of panels) {
        if (otherPanel !== panel) {
          otherPanel.classList.remove("accordion__expanded");
        }
      }
      panel.classList.toggle("accordion__expanded");
    });
  }
}

//slider

const slides = document.querySelector(".slider").children;
const prev = document.querySelector(".controls__prev");
const next = document.querySelector(".controls__next");
const circle = document.querySelector(".circle");

let index = 0;

function circleNavigation() {
  for (let i = 0; i < slides.length; i++) {
    const div = document.createElement("div");
    div.innerHTML = ".";
    div.setAttribute("onClick", "circleSlide(this)");
    div.id = i;
    if (i == 0) {
      div.className = "active";
    }
    circle.appendChild(div);
  }
}

circleNavigation();

function circleSlide(element) {
  index = element.id;
  changeSlide();
  updateCircleNavigation();
}

function updateCircleNavigation() {
  for (let i = 0; i < circle.children.length; i++) {
    circle.children[i].classList.remove("active");
  }
  circle.children[index].classList.add("active");
}

prev.addEventListener("click", () => {
  prevSlide();
  updateCircleNavigation();
});

next.addEventListener("click", () => {
  nextSlide();
  updateCircleNavigation();
});

function prevSlide() {
  if (index == 0) {
    index = slides.length - 1;
  } else {
    index--;
  }
  changeSlide();
}

function nextSlide() {
  if (index == slides.length - 1) {
    index = 0;
  } else {
    index++;
  }
  changeSlide();
}

function changeSlide() {
  for (let i = 0; i < slides.length; i++) {
    slides[i].classList.remove("active");
  }
  slides[index].classList.add("active");
}

function autoPlay() {
  nextSlide();
  updateCircleNavigation();
}

setInterval(() => {
  autoPlay();
}, 10000);

const mybutton = document.getElementById("sticky");

window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 688 ||
    document.documentElement.scrollTop > 688
  ) {
    mybutton.style.display = "flex";
    mybutton.style.justifyContent = "center";
  } else {
    mybutton.style.display = "none";
  }
}

//form

const form = document.getElementById("form");
const name = document.getElementById("name");
const email = document.getElementById("email");
const phone = document.getElementById("phone");
const experiance = document.getElementById("exp");
const organisation = document.getElementById("org");


form.addEventListener("submit", (event) => {
  
  event.preventDefault();

  if($("#form").valid()){
  localStorage.setItem("name", name.value);
  localStorage.setItem("email", email.value);
  localStorage.setItem("phone", phone.value);
  localStorage.setItem("work experiance", experiance.value);
  localStorage.setItem("current organisation", organisation.value);
  alert("We will contact you shortly!");
  } else {
    alert("Error! please modify form as per request")
  }
});


name.value = localStorage.getItem("name");
phone.value = localStorage.getItem("phone");
email.value = localStorage.getItem("email");
experiance.value = localStorage.getItem("work experiance");
organisation.value = localStorage.getItem("current organisation");
